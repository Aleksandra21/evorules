# EVORULES
*Design rules of evolutionary innovation*

### About the project
The main goal of this project is to construct gene expression networks from existing and novel datasets.   
We chose to work with datasets of Saccharomyces Cerevisiae and Drosophila Melanogaster due to their great reputation as model organisms.

### About the repository
This repository contains a toolset for processing raw fastq data into weighted gene correlation networks. It is subdivided into multiple directories.   
The data directory contains data used to test this toolset. The docs directory contains logs of work done and further documentation of the toolkit.   
The scripts directory contains the scripts that can be run to execute the pipeline.

### Prerequisites

Before using this pipeline, a selection of programs is required

To run the first part of the pipeline, from fastq files to count data:
```
* HISAT2 version 2.0.5 or newer
* Python 3.x with the snakemake package installed
* Trimmomatic 0.36 or newer
* A hisat2-indexed reference genome (see https://ccb.jhu.edu/software/hisat2/manual.shtml#the-hisat2-build-indexer   
  for details on how to obtain one of these from fasta files)
* A gtf file with data on your reference genome (most of these can be found at ftp://ftp.ensembl.org/pub/release-95/gtf/)
* fastq-dump 2.8.1 (included in the SRA Toolkit)
* HTSeq version 0.9.1 or newer
* Samtools version 1.3.1 or newer
```

To run the second part of the pipeline, from count data to correlation networks:
```
* GNU R version 3.3.3 or newer, with the following libraries:
	* BiocGenerics, version 0.20.0 or newer
	* pander, version 0.6.3 or newer
	* affy, version 1.52.0 or newer
	* scales, version 0.4.1 or newer
	* pheatmap, 1.0.12 or newer
	* DESeq2, version 1.14.1 or newer
	* biomaRt, version 2.39.2 or newer
	* ggplot2, version 3.1.0 or newer
	* ggrepel, version 0.8.0 or newer
	* WGCNA, version 1.66 or newer
```

Finally, for visualizing the networks, either Cytoscape or VisAnt are needed

### Configuration
For configuration of the pipelines, please refer to their respective readmes.

### How to run

Before using the program, you may want to check the config files and edit the parameters to work for your dataset

After this, running the count data pipeline is as easy as running

```
snakemake --snakefile Pipeline
```

which will then generate count files for you.

To generate networks, you can run
```
Rscript -e "rmarkdown::render('Automated-Pipeline.Rmd', clean=TRUE, output_format='pdf_document')"
```
which also yields a lot of visualizations to assess data quality. This may require a few attempts, due to the instability of the biomaRt library.

## Built With

* [Snakemake](https://snakemake.readthedocs.io/en/stable/) - Pipeline framework
* [HISAT2](https://ccb.jhu.edu/software/hisat2/index.shtml) - Mapping Program
* [Samtools](http://www.htslib.org/) - Interactions with sequencing data
* [Python](https://www.python.org/) - Programming language on which snakemake is built
* [Trimmomatic](http://www.usadellab.org/cms/?page=trimmomatic) - Tool for improving overall read quality by filtering out low quality reads
* [HTSeq](https://htseq.readthedocs.io/en/release_0.11.1/) - A package of tools for analysing high throughput data
* [SRA Toolkit](https://trace.ncbi.nlm.nih.gov/Traces/sra/sra.cgi?view=toolkit_doc&f=std) - A toolkit for dealing with data from the Sequence Read Archive or SRA
* [GNU R](https://www.r-project.org/) - A statistical programming language

## License

This project is licensed under the GNU GPL License - see the [LICENSE.md](LICENSE.md) file for details
