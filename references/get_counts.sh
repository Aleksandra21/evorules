#!/usr/bin/env bash
for file in $(cat $1)
do
	echo $file
	java -jar ~/Documents/RNA-seq\ Practicum/Trimmomatic-0.36/trimmomatic-0.36.jar SE -phred33 "RawData/"$file".fastq.gz" "RawData/"$file"_TRIM.fastq.gz" LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36
	gunzip "RawData/"$file"_TRIM.fastq.gz"
	
	hisat2 -p 1 -S "RawData/"$file"_TRIM.sam" -x "grcm38_tran/genome_tran" "RawData/"$file"_TRIM.fastq"
	
	samtools sort -n "RawData/"$file"_TRIM.sam" -o "RawData/"$file"_TRIM_sn.bam"
	
	samtools view -o "RawData/"$file"_TRIM_sn.sam" "RawData/"$file"_TRIM_sn.bam"
	
	
	
	htseq-count -s no -a 10 "RawData/"$file"_TRIM_sn.sam" GRCm38.91/Mus_musculus.GRCm38.91.gtf > "Counts/"$file".count"
done
