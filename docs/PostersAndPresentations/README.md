##What is this directory for?

This PostersAndPresentations directory contains the following PDF files:

* PresentationSprint1.pdf
* PresentationSecondSprint.pdf
* PresentationSprint3.pdf
* PresentationSprint4.pdf
* Project_poster.pdf
 
This research project was divided into four section (sprints), each pdf file contains results achieved in a sprint. The Project_poster.pdf is a poster with results from the first and second sprint.
