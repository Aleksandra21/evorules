
##What is this directory for?

This directory contains the following subdirectories:

* PostersAndPresentations 
* alignmentSummary 
* benchmarks 

For more information about these directories, please refer to their respective readmes.

Beside the directories it contains the following files:

* Config.R 
* FinalReport.Rmd: The final project report
* Pic_report.png: a network image
* Project report.pdf
* Report.Rmd
* Temp.R: File parser for david clustering output
* iGraph.R: R script that creates a network using iGraph library
* pipelineLog.Rmd
* pipelineLog.pdf
* researchLog.dme.Rmd: research log about the drosophila melanogaster organism
* researchLog.dme.pdf: research log about the drosophila melanogaster organism in PDF format
* researchLog.dre.Rmd: research log about the  danio rerio organism
* researchLog.sce.Rmd:
* sources.bib: bibliography file


