input.dir <- "../data/GSE122470/"

# The GSE accession id for this dataset
GSE.num <- "GSE122470"

# Organism name, shortened. For example, Saccharomyces cerevisiae becomes scerevisiae
organism.name <- "dmelanogaster"

# A data frame showing how the data is built up. Should consist of a number of factors, each denoting a variable
annotation <- data.frame(Time = factor(rep(1:4, each=6),labels = c("DAY_3","DAY_15","DAY_30","DAY_45")))

# The amount of genes, ordered by connectivity, to grab from each module. Set to -1 for unlimited
nTop <- 50;

# The module color to use. "all" yields output for all colors
module <- "all"

# Where to put files made for importing into david
david.out.dir <- "../data/DavidExport/"

# Where the visant-importable files need to go
visant.dir <- "../data/VisAnt/"

# Where the cytoscape-importable files need to go
cytoscape.dir <- "../data/Cytoscape/"

config.design <- ~ Time
