#From SRR to count
This pipeline generates count data from the given SRR accession number. 

###Graphical representation of the pipeline

![](PipelineGraphic/BeforeNormalization.png)

### Prerequisites

* HISAT2 version 2.0.5 or newer
* STAR mapper 2.4.0.1 or newer
* Samtools version 1.3.1
* Python 3 package to create a python3 environment
* Hisat2-indexed reference genome (see link in 'Built With')
* Python3 environment
* Trimmomatic 0.36 or newer
* Gtf file (see link in 'Built With')
* Samtools version 1.3.1 or newer
* fastq-dump 2.8.1 (included in the SRA Toolkit)
* psutil (process and system utilities) library to retrieve running information

### Configuration

trimmomatic:
Contains the path to the trimmomatic jar file

trimmomatic_args:
The arguments to pass to the trimmomatic program.

genome:
The location of a set of hisat2 genome files, including their name (e.g. if the files are named genome_tran.1.ht2, genome_tran.2.ht2, etc, the required name is DIRECTORY/genome_tran)

gtf:
The gtf file to use when counting the expression data. Should be applicable to the genome selected earlier.

samples:
A list of SRR accession numbers to generate count data for (for example, [Drosophila melanogaster;RNA-Seq](https://www.ncbi.nlm.nih.gov/sra/SRX5001546[accn]) would yield SRR8181579 and SRR8181578 as accession numbers)



### Running the program

The following steps will tell how to get this workflow management tool running.

* Go to the directory where the pipeline is located
* Make sure to check the config files and edit the parameters accordingly
* Type the following to run the pipeline:
	
	```
	snakemake --cores [cores you want to use] --snakefile [hisat2Pipeline or starPipeline]
	```
	
### Built With

* [Trimmomatic](http://www.usadellab.org/cms/?page=trimmomatic) - A read trimming toolkit 
* [SRA ToolKit](https://www.ncbi.nlm.nih.gov/sra/docs/toolkitsoft/) - A toolkit to manipulate SRA files
* [R](https://www.r-project.org/about.html) - R is a free software program for statistical computing and graphics
* [Snakemake](https://snakemake.readthedocs.io/en/stable/) -Python-based workflow management tool
* [Genome reference](https://ccb.jhu.edu/software/hisat2/index.shtml) - alignment of next generation sequencing reads to a population of genomes
* [Python](https://www.python.org/) - Programming language on which snakemake is built
* [GTF file](ftp://ftp.ensembl.org/pub/release-95/gtf/) - A file format used to hold information about gene structure
* [Samtools](http://www.htslib.org/) – Suite of program for interacting with high throughput sequencing data
* [HTSeq](https://htseq.readthedocs.io/en/release_0.11.1) - A package for analysing high throughput data
* [psutil](https://pypi.org/project/psutil/) - library to retrieve running information
* [STAR mapper](http://labshare.cshl.edu/shares/gingeraslab/www-data/dobin/STAR/STAR.posix/doc/STARmanual.pdf) - A mapper to maplarge sets of high-throughput sequencing reads to a reference genome. 

## Author

* ** Alexandra Kapielska & Hylke Middel & Marjan Shirzai**

## License

This project is licensed under the GPLv3 License - see the [LICENSE.md](LICENSE.md) file for details
